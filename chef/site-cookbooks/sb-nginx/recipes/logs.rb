#
# Cookbook Name:: sb-nginx
# Recipe:: logs
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe 'sb-logs::logrotate'
