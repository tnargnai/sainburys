#
# Cookbook Name:: sb-nginx
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe 'apt'
include_recipe 'nginx'
include_recipe 'sb-nginx::sites-webapp'
include_recipe 'sb-nginx::monit'
include_recipe 'sb-nginx::logs'
