#
# Cookbook Name:: sb-nginx
# Recipe:: monit
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe 'sb-monit'

monit_check 'nginx' do
  check_id  "/var/run/nginx.pid"
  start     "/etc/init.d/nginx start"
  stop      "/etc/init.d/nginx stop"
  tests [
    {
      'condition' => 'failed host 127.0.0.1 port 80',
      'action'    => 'restart'
    }
  ]
end
