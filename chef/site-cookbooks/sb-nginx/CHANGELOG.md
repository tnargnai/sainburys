sb-nginx CHANGELOG
=================

Cookbook installs/configures NGINX

0.1.0
-----
- [Ian Grant] - Initial release of sb-nginx

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
