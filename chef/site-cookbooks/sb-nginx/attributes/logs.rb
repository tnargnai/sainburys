# sb-nginx::logs
default['sb-logs']['services']['nginx'] = {
 'logrotate' => {
   'logfile'      => '/var/log/nginx/*.log',
   'interval'     => 'daily',
   'rotate_count' => '5',
   'size'         => '200M',
   'owner'        => 'www-data',
   'group'        => 'adm',
   'sharedscripts' => [ '[ -s /run/nginx.pid ] && kill -USR1 `cat /run/nginx.pid`' ]
 }
}
