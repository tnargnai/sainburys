#
# Cookbook Name:: sb-logs
# Recipe:: logrotate
#
# Copyright (c) 2015 MatchesFashion.com, All Rights Reserved.
#
# All rights reserved - Do Not Redistribute
#
# Attributes
#
#   node['logrotate']['services'] = {
#     'varnish'   => { ... },
#     'nginx' => { ... },
#   }
#
#   Configuration Hash:
#   {
#     'logfile':        <str>,
#     'interval':       '(hourly|daily|weekly|monthly|yearly)',
#     'rotate_count':   int,
#     'owner':          <user>,
#     'group':          <group>,
#   }


node['sb-logs']['services'].each { |svc, conf|

  return if node['sb-logs']['services'][svc]['logrotate'].nil?

  conf_d = node['sb-logs']['logrotate']['prefix']

  template "#{conf_d}/#{svc}" do
    action    :create
    owner     'root'
    group     'root'
    mode      0644
    source    'logrotate.conf.erb'
    variables({ :cfg => conf['logrotate'] })
  end
}
