#
# Cookbook Name:: sb-logs
# Recipe:: default
#
# Copyright (c) 2015 MatchesFashion.com, All Rights Reserved.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'sb-logs::log'
include_recipe 'sb-logs::logrotate'
