name             'sb-logs'
maintainer       'Ian Grant'
maintainer_email 'ian.grant@shongo.io'
license          'GPLv2'
description      'Installs/Configures sb-logs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
