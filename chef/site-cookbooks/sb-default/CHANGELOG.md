sb-default CHANGELOG
=================

This file is used to list changes made in each version of the sb-default cookbook.

0.1.0
-----
- [Ian] - Initial release of sb-default cookbook

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
