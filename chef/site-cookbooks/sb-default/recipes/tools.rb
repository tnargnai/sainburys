#
# Cookbook Name:: sh-default
# Recipe:: tools
#
# Copyright 2014, MMatches Fashion
#
# All rights reserved - Do Not Redistribute
#

# Install tools listed in attributes file
node['default']['tools'].each do |pkg|
  package pkg
end
