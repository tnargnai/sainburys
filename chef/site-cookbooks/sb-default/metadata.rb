name             'sb-default'
maintainer       'Ian Grant'
maintainer_email 'ian.grant@shongo.io'
license          'All rights reserved'
description      'Installs/Configures sb-default'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends          'build-essential'
