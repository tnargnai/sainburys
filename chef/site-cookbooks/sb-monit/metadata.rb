name             'sb-monit'
maintainer       'Ian Grant'
maintainer_email 'ian.grant@shongo.io'
license          'all_rights'
description      'Installs/Configures sb-monit'
long_description 'Installs/Configures sb-monit'
version          '0.1.1'

depends          'monit-ng', '= 1.6.2'
