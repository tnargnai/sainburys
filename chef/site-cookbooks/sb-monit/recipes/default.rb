#
# Cookbook Name:: sb-monit
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

# Install Monit
include_recipe 'monit-ng::default'
include_recipe 'monit-ng::config'
