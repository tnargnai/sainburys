sb-monit CHANGELOG
=================

This file is used to list changes made in each version of the sb-ntlm cookbook.

0.1.1
-----
- [Ian Grant] - Upgraded the version of monit-ng to 1.6.2 as 1.6.1 didn't exist on the server and it looks like we're using the 1.6.4 as a default. 1.6.4 is causing errors because of the "verify" method on the template resource not being suported.

0.1.0
-----
- [Ian Grant]  - Initial release of sb-monit

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
