default['monit']['config']['poll_freq']=180
default['monit']['config']['mail_from']='monit@shongo.io'
default['monit']['config']['subscribers']= [ {
    "name" => "ian.grant@shongo.io",
    "subscriptions" => [ "instance", "action", "nonexist" ]
  }
]
default['monit']['config']['mail_servers']= [ {
    "hostname" => "localhost",
    "port" => 25,
    "username" => nil,
    "password" => nil,
    "security" => nil,
    "timeout" => "30 seconds"
  }
]
