name 'sb-goapp'
maintainer 'The Authors'
maintainer       'Ian Grant'
maintainer_email 'ian.grant@shongo.io'
license          'all_rights'
description 'Installs/Configures goapp'
long_description 'Installs/Configures goapp'
version '0.1.0'

# https://github.com/NOX73/chef-golang

depends  'golang'
