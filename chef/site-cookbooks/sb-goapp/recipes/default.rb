#
# Cookbook Name:: goapp
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

# Install 
include_recipe 'golang'

# Install packages
include_recipe 'golang::packages'
