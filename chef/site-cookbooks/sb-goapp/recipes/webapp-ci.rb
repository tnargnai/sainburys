#
# Cookbook Name:: goapp
# Recipe:: webapp-ci
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

# Webapp user
user 'webapp' do
  comment 'User to run webapp'
  shell '/bin/bash'
end

# Create init
cookbook_file '/etc/init/webapp.conf' do
  source 'init/webapp.conf'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

# Set service
service "webapp" do
  supports :status => true, :restart => true
  action [ :enable ]
end

# Create webapp directory
directory "/opt/webapp/latest" do
  owner "webapp"
  group "webapp"
  mode 00755
  recursive true
  action :create
end

# Compile binary from source if source file changes amd start app
execute 'build_webapp' do
  command '/usr/local/go/bin/go build web.go'
  cwd "/opt/webapp/latest"
  user  'webapp'
  group 'webapp'
  action :nothing
  notifies :restart, "service[webapp]"
end

# Create src file and compile if changes
cookbook_file '/opt/webapp/latest/web.go' do
  source 'src/web.go'
  owner 'webapp'
  group 'webapp'
  mode '0755'
  action :create
  notifies :run, 'execute[build_webapp]', :immediately
end
