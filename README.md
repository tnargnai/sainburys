Overview
========

This is the Sainbury's exercise repository.

Set Up
======

We will be using the following for creating our VMs:

* ChefDK - our local Chef environment and related tools
* Vagrant - providing an interface between Chef and Virtual Box
* Virtual Box - hypervisor for creating our VMs  
* Berkshelf - managing our cookbook dependencies (in ChefDK)
* Test Kitchen - tool to create/destroy/test our Chef cookbooks
* Serverspec - writing our tests

Install the following:

ChefDK - https://downloads.chef.io/chef-dk/ and further documentation https://docs.chef.io/install_dk.html

Vagrant - https://docs.vagrantup.com/v2/installation/

Virtual Box - https://www.virtualbox.org/wiki/Downloads

Virtual Box configuration:

All our VMs are configured on the 192.168.33.0 subnet you should have a host network configured with the appropriate settings to allow this.

Gems - ensure kitchen-sync gem is installed ($ gem install kitchen-sync)

Berkshelf
=========

We are using Berkshelf to manage our cookbook dependencies. Before you can start to use Test Kitchen you must create a Berksfile.lock file.

    $ cd ~/chef
    $ berks install
    $ berks update

Test Kitchen
============

We use Test Kitchen to create our VMs and test our cookbooks.

.kitchen.yml - contains all our VM settings and predefined types of VMs. You can list them with:

    $ cd ~/chef
    $ kitchen list

Create a VM

    $ kitchen converge <server>

  eg.

    $ kitchen converge nginx-server-ubuntu-1404

Login to VM

    $ kitchen converge <server>
    $ kitchen login <server>

  eg.

    $ kitchen converge nginx-server-ubuntu-1404
    $ kitchen login nginx-server-ubuntu-1404

Destroy a VM

    $ kitchen destroy <server>

  eg.

    $ kitchen destroy nginx-server-ubuntu-1404

Test a VM

    $ kitchen test <server>

  eg.

    $ kitchen test nginx-server-ubuntu-1404

Test VM while making cookbook changes

    $ kitchen converge <server>
    $ kitchen verify <server>

make change and repeat

    $ kitchen converge <server>
    $ kitchen verify <server>

Sainsbury's Servers
===================

The exercise is to create 3 servers:

* nginx - listening on port 80 and round robin load balancing to Go web application servers
* 2 x Go web application server

I created two types of web app servers:

1. ones that load a binary
1. ones that take a go source file, compile it and load the binary

To test 1) do the following:

    $ cd ~/chef
    $ kitchen destroy
    $ kitchen converge sb-server[123]-ubuntu-1404

Once completed you will be able to test with

    $ curl http://192.168.33.34

or create a /etc/hosts entry (192.168.33.34	webapp.sb.com) and test with

    $ curl http://webapp.sb.com/

Run the command a couple of times and you'll be directed between app servers

To update the binary, copy the new binary to

    ~/chef/site-cookbooks/sb-goapp/files/default/bin/web

and update the app servers

    $ kitchen converge sb-server[23]-ubuntu-1404

Run curl and you will see the new app

    $ curl http://webapp.sb.com/

To test 2) do the following:

    $ cd ~/chef
    $ kitchen destroy
    $ kitchen converge sb-ci-server[123]-ubuntu-1404

Once completed you will be able to test with

    $ curl http://192.168.33.34

or create a /etc/hosts entry (192.168.33.34	webapp.sb.com) and test with

    $ curl http://webapp.sb.com/

Run the command a couple of times and you'll be directed between app servers

To update the source and create a new binary, copy the new source file to

    ~/chef/site-cookbooks/sb-goapp/files/default/src/web.go

and update the app servers

    $ kitchen converge sb-server[23]-ubuntu-1404

Run curl and you will see the new app

    $ curl http://webapp.sb.com/

Testing individual servers

Nginx

    $ kitchen destroy nginx-server-ubuntu-1404
    $ kitchen test nginx-server-ubuntu-1404

Go Web App (binary)

    $ kitchen destroy goapp-server-ubuntu-1404
    $ kitchen test goapp-server-ubuntu-1404

Go Web App (src)

    $ kitchen destroy goapp-ci-server-ubuntu-1404
    $ kitchen test goapp-ci-server-ubuntu-1404